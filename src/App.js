import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container} from 'reactstrap';
import Productos from './Productos.jsx';
function App() {
  return (
    <>
    <Container>
     <h2>Fruiteria</h2>
     <Productos/>
     </Container>
    </>
  );
}

export default App;
