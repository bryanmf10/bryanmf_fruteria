import React, { useState, useEffect } from 'react';
import { Container } from 'reactstrap';

import PRODUCTES from './datos.json';
import Ticket from './Ticket';

export default () =>{

    const [productos, setProductos] = useState(PRODUCTES);
    const [contador, setContador] = useState(0);

    
    const sumar = (x) => {
        const temp = [...PRODUCTES];
        setContador(contador+(temp[x-1].preu));
        
    }

    const restar = (x) => {
        const temp = [...PRODUCTES];
        if(contador> 0){
            setContador(contador-(temp[x-1].preu));
        }   
    }

    return(
        <>
        <Container>
            <h2>Total: {contador} €</h2>
            <Ticket datos={productos} sumador={sumar}  restador={restar} temp={contador}></Ticket>
        </Container>
        </>
    );
}