import React from "react";
import { Button } from "reactstrap";
import styled from 'styled-components';

export default (props) => {
  
  const productos = props.datos.map((el) => (
    <div key={el.id}>
      
      <div>{el.nom} ({el.preu} €/u)</div>
      
      <Button color="secondary" onClick={() => props.sumador(el.id)}>Afegir</Button>
      <Button color="secondary" onClick={() => props.restador(el.id)}>Treure</Button>
      
    </div>
  ));

  return (
    <>
      {productos} 
    </>
  );
};
